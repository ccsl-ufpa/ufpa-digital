import QtQuick 2.14

ListModel {
    ListElement {
        name: "PROAD"
        complement: qsTr("Administração")
        phone: "(91) 3201-7125"
        email: "proad@ufpa.br"
        site: "https://proad.ufpa.br"
    }
    ListElement {
        name: "PROEG"
        complement: qsTr("Ensino")
        phone: "(91) 3201-7129"
        email: "proeg@ufpa.br"
        site: "http://proeg.ufpa.br"
    }
    ListElement {
        name: "PROPESP"
        complement: qsTr("Pesquisa")
        phone: "(91) 3201-7122"
        email: "propesp@ufpa.br"
        site: "http://propesp.ufpa.br"
    }
    ListElement {
        name: "PROEX"
        complement: qsTr("Extensão")
        phone: "(91) 3201-7127"
        email: "proex@ufpa.br"
        site: "http://novoproex.ufpa.br"
    }
    ListElement {
        name: "PROINTER"
        complement: qsTr("Internacional")
        phone: "(91) 3201-7211"
        email: "prointer@ufpa.br"
        site: "http://prointer.ufpa.br"
    }
    ListElement {
        name: "PROGEP"
        complement: qsTr("Pessoal")
        phone: "(91) 3201-7133"
        email: "progep@ufpa.br"
        site: "http://progep.ufpa.br"
    }
    ListElement {
        name: "PROPLAN"
        complement: qsTr("Planejamento")
        phone: "(91) 3201-7120"
        email: "proplan@ufpa.br"
        site: "http://proplan.ufpa.br"
    }
}

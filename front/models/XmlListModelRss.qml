import QtQuick.XmlListModel 2.14

XmlListModel {
    property int start: 0
    property string baseUrl

    function getUrlFull() {
        if (!baseUrl) {
            return ''
        }
        return baseUrl + '&start=' + start
    }

    source: getUrlFull()
    query: '/feed/entry'
    namespaceDeclarations: "declare default element namespace 'http://www.w3.org/2005/Atom';"

    XmlRole {
        name: 'title'
        query: 'title/string()'
        isKey: true
    }
    XmlRole {
        name: 'description'
        query: 'summary/string()'
        isKey: true
    }
    XmlRole {
        name: 'link'
        query: 'id/string()'
        isKey: true
    }
    XmlRole {
        name: 'published'
        query: 'published/string()'
        isKey: true
    }
}

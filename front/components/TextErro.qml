import QtQuick 2.14
import QtQuick.Controls.Material 2.14

Text {
    text: qsTr('Erro ao carregar')
    opacity: 0.5
    color: Material.primaryTextColor
    font.italic: true
    horizontalAlignment: Text.AlignHCenter
    verticalAlignment: Text.AlignVCenter
}

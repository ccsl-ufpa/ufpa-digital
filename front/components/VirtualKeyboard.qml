import QtQuick 2.14
import QtQuick.VirtualKeyboard 2.14

Item {
    id: item
    InputPanel {
        id: inputPanel
        z: 99
        x: 0
        y: item.height
        width: item.width

        states: State {
            name: "visible"
            when: inputPanel.active
            PropertyChanges {
                target: inputPanel
                y: item.height - inputPanel.height
            }
        }
        transitions: Transition {
            from: ""
            to: "visible"
            reversible: true
            ParallelAnimation {
                NumberAnimation {
                    properties: "y"
                    duration: 250
                    easing.type: Easing.InOutQuad
                }
            }
        }
    }
}

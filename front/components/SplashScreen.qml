import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.14

BackgroundAPP {
    ColumnLayout {
        anchors.centerIn: parent
        spacing: 20
        Image {
            id: logo
            Layout.alignment: Qt.AlignCenter
            fillMode: Image.PreserveAspectFit
            source: 'qrc:/static/images/logo-ufpa-white.svg'
            sourceSize.width: 100
            sourceSize.height: 125
        }
        Label {
            id: label
            Layout.alignment: Qt.AlignCenter
            color: 'white'
            text: qsTr('<b>UFPA</b> Digital')
            Component.onCompleted: label.font.pointSize += 6
        }
    }
}
